/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import raw from "../../middleware/route.async.wrapper.mjs";
import express from 'express';
import log from '@ajar/marker';
import connection from '../../db/sql.connection.mjs'
// import {userSchema, usersSchema} from '../../schemas.mjs'

const router = express.Router();

// parse json req.body on post routes
router.use(express.json())

//Get all USERs
router.get( "/",raw(async (req, res) => {
  const [rows, fields] =  await connection.query('SELECT * FROM playground.data;')
  res.status(200).json(rows);
})
);

router.get('/paginate/:page?/:items?', raw( async(req, res)=> {
  let { page = 0 ,items = 10 } = req.params;
  const [rows, fields] =  await connection.query(`SELECT * FROM data LIMIT ${parseInt(items)} OFFSET ${parseInt(page * items)}`)
  res.status(200).json(rows);
}))

// GETS A SINGLE USER
router.get("/:id",raw(async (req, res) => {
  const [rows, fields] =  await connection.query(`SELECT * FROM playground.data WHERE id=${req.params.id}`)
  if(rows.length === 0){
    return res.status(404).send("User not found");
  }else{
    res.status(200).json(rows);
  }}));

//Add single user
router.post("/", raw(async (req, res) => {
    let { first_name ,last_name, email, country} = req.body;
    const [rows, fields] =  await connection.query(`INSERT INTO data (first_name, last_name, email, country) values ("${first_name}", "${last_name}","${email}", "${country}");`)
    res.status(200).json(rows);
}));

// Adds Multiple users
router.post("/add-many", raw(async (req, res) => {
  const valString = req.body.reduce((acc, cur) => {
    if(acc){
      acc += " , "
    }
    return acc + ` ("${cur.first_name}", "${cur.last_name}","${cur.email}", "${cur.country}")`
  }, "");

  const [rows, fields] =  await connection.query(`INSERT INTO data (first_name, last_name, email, country) values ${valString};`)
    res.status(200).json(rows);
}));

// UPDATES A SINGLE USER
router.put("/:id",raw(async (req, res) => {
    let valString = "";
    for(let key of Object.keys(req.body)){
      if(valString){
        valString += " , ";
      }
      valString += ` ${key}="${req.body[key]}"`;
    }
    const [rows, fields] =  await connection.query(`UPDATE data SET ${valString} WHERE id=${req.params.id};
    `)
    res.status(200).json(rows);
})
);


// DELETES A USER
router.delete("/:id",raw(async (req, res) => {
  const [rows, fields] =  await connection.query(`DELETE FROM data WHERE id=${req.params.id};`)
  res.status(200).json(rows);
 
})
);

export default router;
